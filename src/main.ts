import {createApp} from 'vue'
// @ts-ignore
import App from "./App.vue";
import Vant from 'vant';
import * as VueRouter from 'vue-router';
import 'vant/lib/index.css'
import routes from "./config/router.ts";
import '../global.css'
import UUID from 'vue3-uuid'
import { createPinia } from 'pinia'


const pinia = createPinia()
const router = VueRouter.createRouter({
    // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
    history: VueRouter.createWebHashHistory(),
    routes, // `routes: routes` 的缩写
})

const app = createApp(App);
app.use(Vant)
app.use(pinia)
app.use(UUID)
app.use(router)
app.mount('#app')








