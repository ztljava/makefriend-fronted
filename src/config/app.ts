export interface AppState {
  userInfo: string,
  chatUsersList: string,
  currentSessionToUser: string
}

export const appModules: AppState = {
  userInfo: 'userInfo', // 登录信息存储字段-建议每个项目换一个字段，避免与其他项目冲突
  chatUsersList: 'chatUsersList',//消息用户列表
  currentSessionToUser: 'currentSessionToUser', //当前对话对方用户信息
}
