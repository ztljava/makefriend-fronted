// @ts-ignore
import Index from "../pages/Index.vue";
// @ts-ignore
import Team from "../pages/Team.vue";
// @ts-ignore
import User from "../pages/User.vue";
// @ts-ignore
import UserUpdate from "../pages/UserUpdate.vue";
// @ts-ignore
import UserEdit from "../pages/UserEdit.vue";
// @ts-ignore
import UserSearch from "../pages/UserSearch.vue";
// @ts-ignore
import TeamCreate from "../pages/TeamCreate.vue";
// @ts-ignore
import UserSearchResult from "../pages/UserSearchResult.vue";
// @ts-ignore
import UserLogin from "../pages/UserLogin.vue";
import UserCreatedTeam from "../pages/UserCreatedTeam.vue";
import UserJoinTeam from "../pages/UserJoinTeam.vue";
import TeamUpdate from "../pages/TeamUpdate.vue";
import UserRegister from "../pages/UserRegister.vue";
import UploadAvatar from "../pages/UploadAvatar.vue";
import ChatPage from "../pages/chat/ChatPage.vue";

const routes = [
    {path: '/', component: Index},
    {path: '/team', title: '队伍列表页', component: Team},
    {path: '/team/create', title: '创建队伍页', component: TeamCreate},
    {path: '/user', title: '用户页', component: User},
    {path: '/user/update', title: '用户信息页', component: UserUpdate},
    {path: '/user/edit', title: '用户修改页', component: UserEdit},
    {path: '/search', title: '用户搜索页', component: UserSearch},
    {path: '/user/list', title: '用户搜索结果页', component: UserSearchResult},
    {path: '/user/login', title: '用户登录页', component: UserLogin},
    {path: '/user/register', title: '用户注册页', component: UserRegister},
    {path: '/user/team/create', title: '创建队伍', component: UserCreatedTeam},
    {path: '/user/team/join', title: '加入队伍', component: UserJoinTeam},
    {path: '/team/update', title: '修改队伍', component: TeamUpdate},
    {path: '/user/avatar', title: '修改头像', component: UploadAvatar},
    { path: '/chat', title: '聊天', component: ChatPage},
    {path: '/chatPerson', title: '聊天框', component: () => import('../pages/chat/ChatPersonPage.vue')},

]

export default routes;