import { defineStore } from 'pinia';
import { store } from '../index';
import { appModules } from '../../config/app';
import type { AppState } from '../../config/app';

export const useAppStore = defineStore({
  id: 'app',
  state: (): AppState => appModules,
  //开启数据缓存
  persist: {
    enabled: true
  },
  getters: {
    getUserInfo(): string {
      return this.userInfo
    },
    getChatUsersList(): string {
      return this.chatUsersList
    },
    getCurrentSessionToUser(): string {
      return this.currentSessionToUser
    },

  },
  actions: { 

  }
})

export const useAppStoreWithOut = () => {
  return useAppStore(store)
}